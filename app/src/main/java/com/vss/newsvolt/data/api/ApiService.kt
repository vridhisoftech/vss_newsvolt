package com.vss.newsvolt.data.api

import com.vss.newsvolt.BuildConfig
import com.vss.newsvolt.data.cache.entity.ArticleResponse
import com.vss.newsvolt.data.model.BaseResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ApiService {
    //private var API_KEY: String = BuildConfig.News_Api_Key
    @Headers("X-Api-Key: 014a9ff1b39b45c4b68944a56f98ecd6")
    @GET("/v2/top-headlines")
    /*suspend fun getArticles(
        @Query("category") category: String?,
        @Query("country") country: String?,
        @Query("pageSize") pageSize: String?
    ): Call<BaseResponse>*/
    suspend fun getArticles(
            @Query("category") category: String?,
            @Query("country") country: String?,
            @Query("pageSize") pageSize: String?
    ): Response<BaseResponse<List<ArticleResponse>>>

    /*@GET("v2/top-headlines?category=general&pageSize=100&country=in&apiKey=98a6e327b0414e728548f75b1e0bdcd7")
    suspend fun getArticles(): Response<BaseResponse<List<UserResponse>>>*/
}