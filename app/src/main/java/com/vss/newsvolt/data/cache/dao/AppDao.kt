package com.vss.newsvolt.data.cache.dao

import androidx.room.*
import com.vss.newsvolt.data.cache.entity.ArticleEntity
import com.vss.newsvolt.data.cache.entity.BookmarkEntity
import com.vss.newsvolt.data.model.Article

@Dao
interface AppDao {

    /*Main Dao*/
    @Query("SELECT * FROM Article")
    suspend fun getArticles(): List<ArticleEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertArticles(articles: List<ArticleEntity>)

    @Query("SELECT * FROM Article WHERE id = :articleId")
    suspend fun getArticleId(articleId: Long): ArticleEntity

    @Query("DELETE FROM Article")
    suspend fun deleteArticles()

}
