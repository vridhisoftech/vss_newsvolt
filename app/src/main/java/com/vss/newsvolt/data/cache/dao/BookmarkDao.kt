package com.vss.newsvolt.data.cache.dao

import androidx.room.*
import com.vss.newsvolt.data.cache.entity.ArticleEntity
import com.vss.newsvolt.data.cache.entity.BookmarkEntity
import com.vss.newsvolt.data.model.Article

@Dao
interface BookmarkDao {

    @Query("SELECT * FROM Bookmark")
    suspend fun getAllBookmark(): List<BookmarkEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBookmarkArticle(article: BookmarkEntity)

    @Query("DELETE FROM Bookmark WHERE id=:articleId")
    suspend fun deleteBookmarkArticle(articleId: Long)

    @Query("SELECT * FROM Bookmark WHERE id = :articleId")
    suspend fun getBookmarkArticle(articleId: Long): BookmarkEntity
}