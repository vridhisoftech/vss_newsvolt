package com.vss.newsvolt.data.cache.entity

import androidx.annotation.Keep
import com.vss.newsvolt.data.model.Source

@Keep
data class SourceModel(
    val name: String?
)

fun SourceModel.toDomain() = Source(
    name = name
)