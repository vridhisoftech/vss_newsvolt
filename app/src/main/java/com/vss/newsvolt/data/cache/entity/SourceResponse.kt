package com.vss.newsvolt.data.cache.entity

import androidx.annotation.Keep
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class SourceResponse(
    @SerialName("name") val name: String?
)

fun SourceResponse.toData() = SourceModel(
    name = name
)