package com.vss.newsvolt.data.cache.store

import com.vss.newsvolt.data.api.ApiService
import com.vss.newsvolt.data.cache.entity.ArticleModel
import com.vss.newsvolt.data.cache.entity.toData
import com.vss.newsvolt.exception.NotFoundException
import com.vss.newsvolt.utils.NetworkHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class CloudDataStore @Inject constructor(
    private val api: ApiService,
    private val networkHelper: NetworkHelper
) {

    suspend fun getArticles(): List<ArticleModel> = withContext(Dispatchers.IO) {
        if (networkHelper.isInternetOn()) {
            api.getArticles(category = "", country = "in", pageSize = "100")
                .let { response ->
                    response.body()?.articles?.map { it.toData() } ?: throw NotFoundException()
                }
        } else {
            arrayListOf()
        }
    }

    suspend fun getDiscovers(cat: String): List<ArticleModel> = withContext(Dispatchers.IO) {
        if (networkHelper.isInternetOn()) {
            api.getArticles(category = cat, country = "in", pageSize = "100")
                .let { response ->
                    response.body()?.articles?.map { it.toData() } ?: throw NotFoundException()
                }
        } else {
            arrayListOf()
        }
    }
}