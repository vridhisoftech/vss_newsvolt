package com.vss.newsvolt.data.cache.store

import androidx.room.Database
import androidx.room.RoomDatabase
import com.vss.newsvolt.data.cache.dao.AppDao
import com.vss.newsvolt.data.cache.dao.BookmarkDao
import com.vss.newsvolt.data.cache.entity.ArticleEntity
import com.vss.newsvolt.data.cache.entity.BookmarkEntity

@Database(entities = [ArticleEntity::class,BookmarkEntity::class], version = 1, exportSchema = false)
abstract class MainCacheDatabase : RoomDatabase() {
    abstract fun articleDao(): AppDao
    abstract fun bookmarkDao(): BookmarkDao
    companion object {
        val DATABASE_NAME: String = "news_app_db_name"
        /*fun newInstance(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, "my-app.db").build()
        }*/
    }
}