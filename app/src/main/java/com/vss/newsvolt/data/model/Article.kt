package com.vss.newsvolt.data.model

import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize
import android.os.Parcelable
import androidx.annotation.Keep

@Keep
@Parcelize
data class Article(
    @PrimaryKey(autoGenerate = true) var id: Long = 0,
    val author: String?="",
    val content: String?="",
    val description: String?="",
    val publishedAt: String?="",
    val source: Source?,
    val title: String?="",
    val url: String?="",
    val urlToImage: String?="",
    var selected: Boolean?
): Parcelable