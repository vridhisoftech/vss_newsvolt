package com.vss.newsvolt.data.model

import androidx.annotation.Keep

@Keep
open class BaseResponse<T> {
    var articles: T? = null
    var status: String? = null
    var totalResults: Int? = 0
}