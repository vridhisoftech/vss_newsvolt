package com.vss.newsvolt.data.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class Source(
    val name: String?=""
): Parcelable