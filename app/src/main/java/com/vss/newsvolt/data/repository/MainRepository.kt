package com.vss.newsvolt.data.repository

import androidx.annotation.Keep
import com.vss.newsvolt.data.model.Article

@Keep
interface MainRepository {
    suspend fun getArticlesList(refresh: Boolean): List<Article>
    suspend fun getArticle(id: Long): Article
    suspend fun deleteArticle(article: Article)
    suspend fun saveBookmarkArticle(article: Article)
    suspend fun getBookmarkArticle(): List<Article>
    suspend fun getDiscoverArticle(cat: String): List<Article>
    suspend fun deleteBookmarkArticle(articleId: Long)
    suspend fun isBookmarksExist(articleId: Long): Boolean
}