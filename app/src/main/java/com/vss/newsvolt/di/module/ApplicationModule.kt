package com.vss.newsvolt.di.module

import android.content.Context
import androidx.room.Room
import com.vss.newsvolt.BuildConfig
import com.vss.newsvolt.data.api.ApiService
import com.vss.newsvolt.data.cache.dao.AppDao
import com.vss.newsvolt.data.cache.dao.BookmarkDao
import com.vss.newsvolt.data.cache.store.MainCacheDatabase
import com.vss.newsvolt.data.cache.store.PrefHelper
import com.vss.newsvolt.data.cache.store.CloudDataStore
import com.vss.newsvolt.data.cache.store.LocalDataStore
import com.vss.newsvolt.data.repository.MainRepository
import com.vss.newsvolt.data.repository.MainRepositoryImp
import com.vss.newsvolt.utils.NetworkHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object ApplicationModule {

    @Provides
    fun provideBaseUrl() = BuildConfig.BASE_URL

    @Provides
    @Singleton
    fun provideOkHttpClient() = if (BuildConfig.DEBUG) {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.apply { loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY }
        OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()
    } else OkHttpClient
        .Builder()
        .build()


    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        BASE_URL: String
    ): Retrofit =
        Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create())
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .build()

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Provides
    @Singleton
    fun provideMainRepositoryHelper(mainRepository: MainRepositoryImp): MainRepository =
        mainRepository


    @Provides
    @Singleton
    fun provideCacheDb(@ApplicationContext context: Context): MainCacheDatabase {
        return Room.databaseBuilder(
            context, MainCacheDatabase::class.java,
            MainCacheDatabase.DATABASE_NAME
        )
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideArticleDao(mainCacheDatabase: MainCacheDatabase): AppDao = mainCacheDatabase.articleDao()

    @Singleton
    @Provides
    fun provideBookmarkDao(mainCacheDatabase: MainCacheDatabase): BookmarkDao = mainCacheDatabase.bookmarkDao()

    @Singleton
    @Provides
    fun providePrefHelper(@ApplicationContext context: Context): PrefHelper {
        return PrefHelper(context)
    }

    @Singleton
    @Provides
    fun provideMainRepositoryImp(
        cloudDataStore: CloudDataStore,
        localDataStore: LocalDataStore
    ): MainRepositoryImp {
        return MainRepositoryImp(cloudDataStore, localDataStore)
    }

    @Singleton
    @Provides
    fun provideLocalDataStore(appDao: AppDao, bookmarkDao: BookmarkDao, prefHelper: PrefHelper): LocalDataStore =
        LocalDataStore(appDao, bookmarkDao, prefHelper)

    @Singleton
    @Provides
    fun provideCloudDataStore(
        apiService: ApiService,
        networkHelper: NetworkHelper
    ): CloudDataStore = CloudDataStore(apiService, networkHelper)

    @Singleton
    @Provides
    fun provideNetworkHelper(@ApplicationContext context: Context): NetworkHelper =
        NetworkHelper(context)

    /*@Provides
    fun provideListener(listener: StoryFragmentAdapter.StoryAdapterListener): StoryFragmentAdapter.StoryAdapterListener = listener

    @Singleton
    @Provides
    fun provideStoryFragmentAdapter(listener: StoryFragmentAdapter.StoryAdapterListener): StoryFragmentAdapter =
        StoryFragmentAdapter(listener)*/

}