package com.vss.newsvolt.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.vss.newsvolt.data.model.Article
import com.vss.newsvolt.databinding.ItemLayoutCommonBinding
import java.util.*
import kotlin.collections.ArrayList

class CommonFragmentAdapter constructor(
    private val listener: CommonAdapterListener
    ): RecyclerView.Adapter<CommonFragmentAdapter.DataViewHolder>(),
       Filterable {

    private val articlesList = mutableListOf<Article>()
    private var articlesFilterList = mutableListOf<Article>()
    private var hideMore: Boolean? = false

    interface CommonAdapterListener{
        fun onMoreClick(article: Article)
        fun onItemClick(article: Article)
    }

    inner class DataViewHolder(private val binding: ItemLayoutCommonBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(article: Article) {
            binding.news = article
            binding.ivOptions.setOnClickListener { listener.onMoreClick(article)  }
            itemView.setOnClickListener { listener.onItemClick(article)  }
            if(hideMore!!){
                binding.ivOptions.visibility = View.GONE
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder =
        DataViewHolder(
            ItemLayoutCommonBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) =
        holder.bind(articlesFilterList[position])

    override fun getItemCount(): Int = articlesFilterList.size

    fun updateArticlesList(userList: List<Article>, hideMore: Boolean = false) {
        if(articlesFilterList.size == userList.size) return
        this.hideMore = hideMore
        with(articlesFilterList) {
            clear()
            addAll(userList)
        }

        notifyDataSetChanged()
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    articlesFilterList = articlesList
                } else {
                    val resultList = ArrayList<Article>()
                    for (row in articlesList) {
                        if (row.title!!.toLowerCase(Locale.ROOT).contains(charSearch.toLowerCase(Locale.ROOT))) {
                            resultList.add(row)
                        }
                    }
                    articlesFilterList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = articlesFilterList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                articlesFilterList = results?.values as ArrayList<Article>
                notifyDataSetChanged()
            }

        }
    }

}