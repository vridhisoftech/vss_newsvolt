package com.vss.newsvolt.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.vss.newsvolt.data.model.Article
import com.vss.newsvolt.databinding.ItemLayoutBinding

class StoryFragmentAdapter constructor(private val listener: StoryAdapterListener) : RecyclerView.Adapter<StoryFragmentAdapter.DataViewHolder>() {
    private val articles = mutableListOf<Article>()

    interface StoryAdapterListener{
        fun onImageClick(article: Article, imageView: ImageView)
        fun onItemClick(article: Article)
        fun onSaveClick(article: Article, imageView: ImageView)
        fun onShareClick(article: Article)
    }

    inner class DataViewHolder(private val binding: ItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(article: Article) {
            binding.article = article
            binding.ivSave.setOnClickListener { listener.onSaveClick(article, binding.ivSave) }
            binding.ivShare.setOnClickListener { listener.onShareClick(article) }
            binding.imageView.setOnClickListener { listener.onImageClick(article, binding.imageView) }
            binding.seemore.setOnClickListener { listener.onItemClick(article)  }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder =
        DataViewHolder(
            ItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) =
        holder.bind(articles[position])

    override fun getItemCount(): Int = articles.size

    fun updateArticlesList(list: List<Article>) {
        if(articles.size == list.size) return
        with(articles) {
            clear()
            addAll(list)
        }

        notifyDataSetChanged()
    }

}