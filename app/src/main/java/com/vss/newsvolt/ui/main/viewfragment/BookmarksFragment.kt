package com.vss.newsvolt.ui.main.viewfragment

import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.vss.newsvolt.R
import com.vss.newsvolt.data.model.Article
import com.vss.newsvolt.databinding.FragmentBookmarksBinding
import com.vss.newsvolt.ui.main.adapter.CommonFragmentAdapter
import com.vss.newsvolt.ui.main.viewmodel.BookmarksViewModel
import com.vss.newsvolt.utils.Status
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BookmarksFragment: BaseFragment(R.layout.fragment_bookmarks), CommonFragmentAdapter.CommonAdapterListener {

    private var adapter: CommonFragmentAdapter? = null

    private val viewModel: BookmarksViewModel by viewModels()

    private lateinit var mBinding: FragmentBookmarksBinding

    override fun initView(view: View) {
        val binding = FragmentBookmarksBinding.bind(view)
        mBinding = binding
        setupUi()
        viewModel.fetchBookmarkArticles()
    }

    override fun initData() {
        setupObserver()
    }

    override fun onMoreClick(article: Article) {
        val bottomSheet = OptionsBottomSheet.getInstance(
            article.title,
            article.url,
            article.id
        )
        bottomSheet?.setOnItemClickListner(object : OptionsBottomSheet.OptionsBottomSheetListener {
            override fun onRemove(id: Long?) {
               viewModel.deleteBookmark(id!!)
                viewModel.fetchBookmarkArticles()
            }
        })

        bottomSheet?.show(requireActivity().supportFragmentManager, bottomSheet.tag)
    }

    override fun onItemClick(article: Article) {
        val action =
            BookmarksFragmentDirections.actionBookmarksFragmentToDetailFragment(param = article)
        findNavController().navigate(action)
    }

    private fun setupUi() {
        if(adapter == null) adapter = CommonFragmentAdapter(this)
        this.let { it ->
            mBinding.apply {
                viewModel = it.viewModel
                //mBinding.recyclerView.setHasFixedSize(true)
                mBinding.recyclerView.layoutManager = LinearLayoutManager(
                    requireContext(),
                    LinearLayoutManager.VERTICAL,
                    false
                )
                mBinding.recyclerView.adapter = adapter
                adapter?.notifyDataSetChanged()
            }
        }

        mBinding.searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter?.filter?.filter(newText)
                return false
            }
        })
    }

    private fun setupObserver() {
        viewModel.bookmark.observe(this@BookmarksFragment, Observer { status ->
            when (status) {
                is Status.Loading -> {
                    displayView(true, mBinding.progressBar)
                    displayView(false, mBinding.warning)
                }
                is Status.Error -> {
                    displayError(status.exception.message)
                    displayView(true, mBinding.warning)
                    displayView(false, mBinding.progressBar)
                    mBinding.swipeContainer.isRefreshing = false
                }
                is Status.Success -> {
                    displayView(false, mBinding.warning)
                    displayView(false, mBinding.progressBar)
                    renderList(status.data)
                    mBinding.swipeContainer.isRefreshing = false
                }
            }
        })
    }

    private fun displayError(message: String?) {
        if (message != null) mBinding.warning.text = message else mBinding.warning.text =
            "Unknown error."
    }

    private fun displayView(isDisplayed: Boolean, view: View) {
        view.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    private fun renderList(users: List<Article>) {
        adapter?.updateArticlesList(users)
        if(users.isNotEmpty()){
            displayView(false, mBinding.warning)
        }else{
            displayView(true, mBinding.warning)
        }

    }
}