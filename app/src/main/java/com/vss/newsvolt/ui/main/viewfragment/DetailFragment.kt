package com.vss.newsvolt.ui.main.viewfragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.vss.newsvolt.R
import com.vss.newsvolt.data.model.Article
import com.vss.newsvolt.databinding.FragmentDetailBinding
import dagger.hilt.android.AndroidEntryPoint
import java.security.spec.ECField

@AndroidEntryPoint
class DetailFragment : Fragment(R.layout.fragment_detail) {

    private lateinit var mBinding: FragmentDetailBinding
    private val args: DetailFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentDetailBinding.bind(view)
        mBinding = binding
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mBinding.article = args.param

        mBinding.seemore.setOnClickListener {
            try {
                val action =
                    DetailFragmentDirections.actionDetailFragmentToWebViewFragment(param = args.param)
                requireActivity().findNavController(R.id.nav_host_container).navigate(action)
            }catch (e: Exception){}
        }
        mBinding.ivClose.setOnClickListener {
            it.findNavController().popBackStack(R.id.detail_fragment, true)
        }
        mBinding.image.setOnClickListener {
            try {
                val action = DetailFragmentDirections.actionDetailFragmentToGalleryFragment(param = args.param.urlToImage!!)
                requireActivity().findNavController(R.id.nav_host_container).navigate(action)
            } catch (e: Exception) {
            }
        }
        mBinding.ivShare.setOnClickListener { setupShareButton(article = args.param)  }
    }

    private fun setupShareButton(article: Article) {
        val intent = Intent(Intent.ACTION_SEND)
        val shareText: String = article.title.toString() + "\n" + article.url
        intent.putExtra(Intent.EXTRA_TEXT, shareText)
        intent.type = "text/plain"
        startActivity(intent)
    }
}

