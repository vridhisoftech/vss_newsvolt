package com.vss.newsvolt.ui.main.viewfragment

import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.vss.newsvolt.R
import com.vss.newsvolt.data.model.Article
import com.vss.newsvolt.databinding.FragmentDiscoverBinding
import com.vss.newsvolt.ui.main.adapter.CommonFragmentAdapter
import com.vss.newsvolt.ui.main.viewmodel.DiscoverViewModel
import com.vss.newsvolt.utils.Status
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class DiscoverFragment : BaseFragment(R.layout.fragment_discover), CommonFragmentAdapter.CommonAdapterListener,
    OnItemSelectedListener {

    private var adapter: CommonFragmentAdapter? = null

    private var spAdapter: ArrayAdapter<*>? = null

    private val viewModel: DiscoverViewModel by viewModels()

    private lateinit var mBinding: FragmentDiscoverBinding

    override fun initView(view: View) {
        val binding = FragmentDiscoverBinding.bind(view)
        mBinding = binding
        setupUi()
    }

    override fun initData() {
        setupObserver()
    }

    override fun onMoreClick(article: Article) {
        /*val bottomSheet = OptionsBottomSheet.getInstance(
            article.title,
            article.url,
            article.id.toInt(),
            true
        )
        bottomSheet?.show(requireActivity().supportFragmentManager, bottomSheet.tag)*/
    }

    override fun onItemClick(article: Article) {
        val action =
            DiscoverFragmentDirections.actionDiscoverFragmentToDetailFragment(param = article)
        findNavController().navigate(action)
    }

    private fun setupUi() {
        if(spAdapter == null) spAdapter  = ArrayAdapter.createFromResource(
            requireContext(),
            R.array.items_class,
            R.layout.simple_spinner_dropdown_item
        )

        mBinding.spinner.onItemSelectedListener = this

        spAdapter?.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        mBinding.spinner.adapter = spAdapter
        spAdapter?.notifyDataSetChanged()

        if(adapter == null) adapter = CommonFragmentAdapter(this)
        this.let { it ->
            mBinding.apply {
                viewModel = it.viewModel
                mBinding.recyclerView.setHasFixedSize(true)
                mBinding.recyclerView.layoutManager = LinearLayoutManager(
                    requireContext(),
                    LinearLayoutManager.VERTICAL,
                    false
                )
                mBinding.recyclerView.adapter = adapter
                adapter?.notifyDataSetChanged()
            }
        }

        mBinding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter?.filter?.filter(newText)
                return false
            }
        })
    }

    private fun setupObserver() {
        viewModel.discover.observe(this@DiscoverFragment, Observer { status ->
            when (status) {
                is Status.Loading -> {
                    displayView(true, mBinding.progressBar)
                    displayView(false, mBinding.warning)
                }
                is Status.Error -> {
                    displayError(status.exception.message)
                    displayView(true, mBinding.warning)
                    displayView(false, mBinding.progressBar)
                }
                is Status.Success -> {
                    renderList(status.data)
                    displayView(false, mBinding.warning)
                    displayView(false, mBinding.progressBar)
                }
            }
        })
    }

    private fun displayError(message: String?) {
        if (message != null) mBinding.warning.text = message else mBinding.warning.text =
            "Unknown error."
    }

    private fun displayView(isDisplayed: Boolean, view: View) {
        view.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    private fun renderList(users: List<Article>) {
        adapter?.updateArticlesList(users, true)
        if(users.isNotEmpty()){
            displayView(false, mBinding.warning)
        }else{
            displayView(true, mBinding.warning)
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val item: Any? = parent?.getItemAtPosition(position)
        viewModel.fetchDiscoverArticles(item.toString().toLowerCase())
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}
}