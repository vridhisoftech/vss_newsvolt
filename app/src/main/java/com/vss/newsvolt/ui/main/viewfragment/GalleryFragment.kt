package com.vss.newsvolt.ui.main.viewfragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import androidx.transition.TransitionInflater
import com.vss.newsvolt.R
import com.vss.newsvolt.databinding.FragmentGalleryBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GalleryFragment : Fragment(R.layout.fragment_gallery) {
    private lateinit var mBinding: FragmentGalleryBinding
    private val args: GalleryFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentGalleryBinding.bind(view)
        mBinding = binding
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mBinding.imageView.apply {
            transitionName = "imageView"
            Glide.with(this@GalleryFragment)
                .load(args.param)
                .apply(RequestOptions.noTransformation())
                .into(this)
        }
       // mBinding.urlToImage = args.param
        mBinding.ivClose.setOnClickListener { it.findNavController().popBackStack(R.id.gallery_fragment, true) }
    }
}