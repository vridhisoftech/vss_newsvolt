package com.vss.newsvolt.ui.main.viewfragment

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.navigation.findNavController
import com.vss.newsvolt.BuildConfig
import com.vss.newsvolt.R
import com.vss.newsvolt.data.model.Article
import com.vss.newsvolt.data.model.Source
import com.vss.newsvolt.databinding.FragmentSettingsBinding

class SettingsFragment : BaseFragment(R.layout.fragment_settings) {


    override fun initData() {

    }

    override fun initView(view: View) {
        val binding = FragmentSettingsBinding.bind(view)
        binding.share.setOnClickListener {
            val uri: Uri = Uri.parse("http://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}")
             shareApp(uri)
        }
        binding.rate.setOnClickListener {
            rateApp()
        }
        binding.feedback.setOnClickListener {
            try {
                val article = Article(0,"","","","",
                    Source("Feedback"),"","https://www.vridhisoftech.com/contact/","", false)
                val action = SettingsFragmentDirections.actionSettingsFragmentToWebViewFragment(param = article)
                requireActivity().findNavController(R.id.nav_host_container).navigate(action)
            }catch (e: Exception){}
        }
        binding.tnc.setOnClickListener {
            try {
                val article = Article(0,"","","","",
                    Source("Terms & Conditions"),"","file:///android_asset/tnc.html","", false)
                val action = SettingsFragmentDirections.actionSettingsFragmentToWebViewFragment(param = article)
                requireActivity().findNavController(R.id.nav_host_container).navigate(action)
            }catch (e: Exception){}
        }
        binding.privacy.setOnClickListener {
            try {
                val article = Article(0,"","","","",
                    Source("Privacy Policy"),"","file:///android_asset/privacy.html","", false)
                val action = SettingsFragmentDirections.actionSettingsFragmentToWebViewFragment(param = article)
                requireActivity().findNavController(R.id.nav_host_container).navigate(action)
            }catch (e: Exception){}
        }
    }

    private fun rateApp(){
        val uri: Uri = Uri.parse("market://details?id=${BuildConfig.APPLICATION_ID}")
        val goToMarket = Intent(Intent.ACTION_VIEW, uri)
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
        try {
            startActivity(goToMarket)
        } catch (e: ActivityNotFoundException) {
            startActivity(Intent(Intent.ACTION_VIEW,
                Uri.parse("http://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}")))
        }
    }

    private fun shareApp(uri: Uri) {
        val intent = Intent(Intent.ACTION_SEND)
        val shareText: String = "Download this app:\n$uri"
        intent.putExtra(Intent.EXTRA_TEXT, shareText)
        intent.type = "text/plain"
        startActivity(intent)
    }
}