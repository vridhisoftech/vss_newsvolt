package com.vss.newsvolt.ui.main.viewfragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.vss.newsvolt.R
import com.vss.newsvolt.data.model.Article
import com.vss.newsvolt.databinding.FragmentStoryViewBinding
import com.vss.newsvolt.ui.main.adapter.StoryFragmentAdapter
import com.vss.newsvolt.ui.main.viewmodel.MainViewModel
import com.vss.newsvolt.utils.Status
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class StoryViewFragment : BaseFragment(R.layout.fragment_story_view), StoryFragmentAdapter.StoryAdapterListener {

    private var adapter: StoryFragmentAdapter? = null

    private val mainViewModel: MainViewModel by viewModels()

    private lateinit var mBinding: FragmentStoryViewBinding

    private var ivSave: ImageView? = null

    private var mArticle: Article? = null

    private var bookmarksList: List<Article>? = null

    override fun initView(view: View) {
        val binding = FragmentStoryViewBinding.bind(view)
        mBinding = binding
        setupUi()
        mainViewModel.fetchData()
    }

    override fun initData() {
        setupObserver()
    }

    override fun onImageClick(article: Article, imageView: ImageView) {
        val extras = FragmentNavigatorExtras(imageView to "imageView")
        val action = StoryViewFragmentDirections.actionStoryViewFragmentToGalleryFragment(param = article.urlToImage!!)
        findNavController().navigate(action, extras)
    }

    override fun onItemClick(article: Article) {
        val action =
            StoryViewFragmentDirections.actionStoryViewFragmentToWebViewFragment(param = article)
        findNavController().navigate(action)
    }

    override fun onSaveClick(article: Article, imageView: ImageView) {
            ivSave = imageView
            mArticle = article
            mainViewModel.checkIsBookmarkExist(article.id)
    }

    override fun onShareClick(article: Article) {
        setupShareButton(article)
    }

    private fun setupShareButton(article: Article) {
        val intent = Intent(Intent.ACTION_SEND)
        val shareText: String = article.title.toString() + "\n" + article.url
        intent.putExtra(Intent.EXTRA_TEXT, shareText)
        intent.type = "text/plain"
        startActivity(intent)
    }


    private fun setupUi() {
           if(adapter == null) adapter = StoryFragmentAdapter(this)
            this.let { it ->
                mBinding.apply {
                    viewModel = it.mainViewModel
                    mBinding.viewPager.offscreenPageLimit = 6
                    mBinding.viewPager.adapter = adapter
                    mBinding.viewPager.overScrollMode = View.OVER_SCROLL_NEVER
                    adapter?.notifyDataSetChanged()
                    mBinding.viewPager.registerOnPageChangeCallback(object :
                        ViewPager2.OnPageChangeCallback() {
                        private var mCurItem = 0
                        override fun onPageScrolled(
                            position: Int,
                            positionOffset: Float,
                            positionOffsetPixels: Int
                        ) {
                            super.onPageScrolled(position, positionOffset, positionOffsetPixels)
                            if (position == mCurItem) return

                            /**
                             * VerticalViewPager Whether to slide backwards
                             */
                            val mIsReverseScroll = position < mCurItem
                        }

                        override fun onPageSelected(position: Int) {
                            super.onPageSelected(position)
                            if (position == mCurItem) return
                        }

                        override fun onPageScrollStateChanged(state: Int) {
                            super.onPageScrollStateChanged(state)
                            val SCROLL_STATE_DRAGGING = 1
                            if (state == SCROLL_STATE_DRAGGING) {
                                mCurItem = mBinding.viewPager.currentItem
                            }
                        }
                    })
                }
        }
    }

    private fun setupObserver() {

        mainViewModel.bookmark.observe(this@StoryViewFragment, Observer { it ->
            bookmarksList = it
        })

        mainViewModel.articles.observe(this@StoryViewFragment, Observer { status ->
            when (status) {
                is Status.Loading -> {
                    mBinding.swipeContainer.isRefreshing = true
                    displayView(true, mBinding.progressBar)
                    displayView(false, mBinding.warning)
                }
                is Status.Error -> {
                    displayError(status.exception.message)
                    mBinding.swipeContainer.isRefreshing = false
                    displayView(true, mBinding.warning)
                    displayView(false, mBinding.progressBar)
                }
                is Status.Success -> {
                    mBinding.swipeContainer.isRefreshing = false
                    displayView(false, mBinding.warning)
                    displayView(false, mBinding.progressBar)
                    renderList(status.data)
                }
            }
        })


        mainViewModel.isExist.observe(this@StoryViewFragment, Observer { it ->
            if (it == true) {
                ivSave?.post {
                    ivSave?.setImageDrawable(
                        ContextCompat.getDrawable(
                            requireContext(),
                            R.drawable.ic_save
                        )
                    )
                }
                mainViewModel.deleteBookmark(mArticle!!.id)
            } else {
                ivSave?.post {
                    ivSave?.setImageDrawable(
                        ContextCompat.getDrawable(
                            requireContext(),
                            R.drawable.ic_saved
                        )
                    )
                }
                mainViewModel.saveBookmark(mArticle!!)
            }
        })
    }

    private fun displayError(message: String?) {
        if (message != null) mBinding.warning.text = message else mBinding.warning.text =
            "Unknown error."
    }

    private fun displayView(isDisplayed: Boolean, view: View) {
        view.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    private fun renderList(articles: List<Article>) {
        articles.forEachIndexed { _, article ->
            bookmarksList!!.forEachIndexed { _, bookmark ->
                if (article.title.equals(bookmark.title, true)) {
                    article.selected = true
                }
            }
        }
        adapter?.updateArticlesList(articles)
        if(articles.isNotEmpty()){
            displayView(false, mBinding.warning)
        }else{
            displayView(true, mBinding.warning)
        }
    }
}