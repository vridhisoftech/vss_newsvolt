package com.vss.newsvolt.ui.main.viewfragment

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.webkit.WebResourceResponse
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.vss.newsvolt.R
import com.vss.newsvolt.databinding.FragmentWebViewBinding
import com.vss.newsvolt.utils.AdBlocker
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WebViewFragment : Fragment(R.layout.fragment_web_view) {

    private lateinit var mBinding: FragmentWebViewBinding
    private val args: WebViewFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentWebViewBinding.bind(view)
        mBinding = binding
        setUpWebview()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUi()
    }

    private fun setUpWebview() {
        val url: String = args.param.url!!
        mBinding.webView.loadUrl(url)
    }

    private fun setupUi() {
        mBinding.back.setOnClickListener { requireActivity().onBackPressed() }
        mBinding.title.text = args.param.source?.name
        mBinding.refresh.setOnClickListener { mBinding.webView.reload() }
        val webSettings: WebSettings = mBinding.webView.settings
        webSettings.javaScriptEnabled = true
        webSettings.javaScriptCanOpenWindowsAutomatically = true
        webSettings.domStorageEnabled = true
        mBinding.webView.webViewClient = MyWebViewClient(mBinding.progressBar)
        mBinding.webView.setOnCreateContextMenuListener(this)
    }

    class MyWebViewClient(private val progressBar: ProgressBar) : WebViewClient() {
        override fun onPageFinished(view: WebView, url: String) {
            progressBar.visibility = View.INVISIBLE
        }
        override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            progressBar.visibility = View.VISIBLE
        }
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            return if (url.endsWith(".mp4")) {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.setDataAndType(Uri.parse(url), "video/*")
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                view.context.startActivity(intent)
                true
            } else if (url.startsWith("tel:") || url.startsWith("sms:") || url.startsWith("smsto:")
                || url.startsWith("mms:") || url.startsWith("mmsto:")
            ) {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                view.context.startActivity(intent)
                true
            } else {
                super.shouldOverrideUrlLoading(view, url)
            }
        }

        private val loadedUrls: MutableMap<String, Boolean> = java.util.HashMap()
        override fun shouldInterceptRequest(view: WebView, url: String): WebResourceResponse? {
            val ad: Boolean
            if (!loadedUrls.containsKey(url)) {
                ad = AdBlocker.isAd(url)
                loadedUrls[url] = ad
            } else {
                ad = loadedUrls[url]!!
            }
            return if (ad) AdBlocker.createEmptyResource() else super.shouldInterceptRequest(
                view,
                url
            )
        }
    }

}