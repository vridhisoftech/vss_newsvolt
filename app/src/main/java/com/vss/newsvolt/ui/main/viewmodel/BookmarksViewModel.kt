package com.vss.newsvolt.ui.main.viewmodel


import androidx.databinding.ObservableBoolean
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vss.newsvolt.data.model.Article
import com.vss.newsvolt.data.repository.MainRepository
import com.vss.newsvolt.utils.Status
import kotlinx.coroutines.launch

class BookmarksViewModel @ViewModelInject constructor(
    private val repository: MainRepository
) : ViewModel() {

    private val _bookmarks = MutableLiveData<Status<List<Article>>>()
    val bookmark: LiveData<Status<List<Article>>> get() = _bookmarks

    val isLoading = ObservableBoolean(false)


    fun fetchBookmarkArticles() {
        viewModelScope.launch {
            try {
                _bookmarks.postValue(Status.Loading)
                isLoading.set(true)
                val bookmarks = repository.getBookmarkArticle()
                isLoading.set(false)
                _bookmarks.postValue(Status.Success(bookmarks))
            } catch (e: Exception) {
                isLoading.set(false)
                _bookmarks.postValue(Status.Error(e))
            }
        }
    }

    fun deleteBookmark(id: Long) {
        viewModelScope.launch {
            try {
                repository.deleteBookmarkArticle(id)
            } catch (e: java.lang.Exception) {

            }
        }
    }
}