package com.vss.newsvolt.ui.main.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vss.newsvolt.data.model.Article
import com.vss.newsvolt.data.repository.MainRepository
import com.vss.newsvolt.utils.Status
import kotlinx.coroutines.launch

class DiscoverViewModel @ViewModelInject constructor(
    private val repository: MainRepository
) : ViewModel() {

    private val _discover = MutableLiveData<Status<List<Article>>>()
    val discover: LiveData<Status<List<Article>>> get() = _discover

    val isLoading = ObservableBoolean(false)

    fun fetchDiscoverArticles() {
        fetchDiscoverArticles("")
    }

    fun fetchDiscoverArticles(cat: String? = "general") {
        viewModelScope.launch {
            try {
                _discover.postValue(Status.Loading)
                isLoading.set(true)
                val bookmarks = repository.getDiscoverArticle(cat!!)
                isLoading.set(false)
                _discover.postValue(Status.Success(bookmarks))
            }catch (e: Exception){
                isLoading.set(false)
                _discover.postValue(Status.Error(e))
            }
        }
    }
}