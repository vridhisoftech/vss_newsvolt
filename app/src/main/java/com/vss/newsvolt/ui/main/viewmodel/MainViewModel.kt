package com.vss.newsvolt.ui.main.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vss.newsvolt.data.model.Article
import com.vss.newsvolt.data.repository.MainRepository
import com.vss.newsvolt.utils.Status
import kotlinx.coroutines.launch

class MainViewModel @ViewModelInject constructor(
    private val repository: MainRepository
) : ViewModel() {

    private val _articles = MutableLiveData<Status<List<Article>>>()
    val articles: LiveData<Status<List<Article>>> get() = _articles

    private val _bookmarks = MutableLiveData<List<Article>>()
    val bookmark: LiveData<List<Article>> get() = _bookmarks

    private val _isExist = MutableLiveData<Boolean>()
    val isExist: LiveData<Boolean> get() = _isExist

    val isLoading = ObservableBoolean(false)

    fun fetchData(refresh: Boolean = false){
        fetchBookmarks()
        fetchArticles(refresh)
    }

   private fun fetchArticles(refresh: Boolean) {
        viewModelScope.launch {
            try {
                _articles.postValue(Status.Loading)
                isLoading.set(true)
                val articles = repository.getArticlesList(refresh)
                isLoading.set(false)
                _articles.postValue(Status.Success(articles))
            } catch (e: Exception) {
                isLoading.set(false)
                _articles.postValue(Status.Error(e))
            }
        }
    }

    private fun fetchBookmarks(){
        viewModelScope.launch {
            try {
                val bookmarks = repository.getBookmarkArticle()
                _bookmarks.postValue(bookmarks)
            }catch (e: Exception){
                e.printStackTrace()
            }
        }
    }

    fun saveBookmark(article: Article) {
        viewModelScope.launch {
            repository.saveBookmarkArticle(article)
        }
    }

    fun deleteBookmark(articleId: Long) {
        viewModelScope.launch {
            repository.deleteBookmarkArticle(articleId)
        }
    }

    fun checkIsBookmarkExist(articleId: Long){
        viewModelScope.launch {
           _isExist.postValue(repository.isBookmarksExist(articleId))
            fetchBookmarks()
        }
    }

}